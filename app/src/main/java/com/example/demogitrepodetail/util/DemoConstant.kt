package com.example.demogitrepodetail.util

object DemoConstant {
    const val BASE_URL = "https://api.github.com/"
    const val URL_SEARCH =
        "https://api.github.com/search/repositories?q=100"
    val URL_ITEM = "https://api.github.com/repos/"
}