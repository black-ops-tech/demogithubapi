package com.example.demogitrepodetail.model

data class DataModel(
    val incomplete_results: Boolean,
    val items: List<Item>,
    val total_count: Int
)