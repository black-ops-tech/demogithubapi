package com.example.demogitrepodetail.repository

import com.example.demogitrepodetail.network.DemoService
import javax.inject.Inject

class DemoRepository @Inject constructor(private val demoService: DemoService) {
    fun getApiRepoData() = demoService.getData()
}