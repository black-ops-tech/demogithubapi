package com.example.demogitrepodetail.repository

import dagger.hilt.android.scopes.ActivityRetainedScoped
import javax.inject.Inject

@ActivityRetainedScoped
class Repository @Inject constructor(private val demoRepository: DemoRepository) {
    fun getDemoData() = demoRepository.getApiRepoData()
}

