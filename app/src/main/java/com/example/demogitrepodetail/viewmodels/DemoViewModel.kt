package com.example.demogitrepodetail.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import com.example.demogitrepodetail.model.DataModel
import com.example.demogitrepodetail.repository.DemoRepository
import com.example.demogitrepodetail.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class DemoViewModel @Inject constructor(
    private val repository: Repository, application: Application
) : AndroidViewModel(application){


    fun fetchData() {
        repository.getDemoData().enqueue(object : Callback<DataModel> {
            override fun onResponse(call: Call<DataModel>, response: Response<DataModel>) {
                println("RESPONSE::" + response.body());
//                for(a in response.body()!!.items){
//
//                }
                println("RESPONSE:: "+response.body()!!.items.size)
            }

            override fun onFailure(call: Call<DataModel>, t: Throwable) {
                Log.d("TAG", t.message.toString())
            }
        })
    }
}