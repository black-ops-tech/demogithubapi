package com.example.demogitrepodetail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.example.demogitrepodetail.databinding.ActivityMainBinding
import com.example.demogitrepodetail.viewmodels.DemoViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val demoViewModel by viewModels<DemoViewModel>()
    private lateinit var _binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        fetchDataOfApi()
    }

    private fun fetchDataOfApi() {
        demoViewModel.fetchData()
//        mainViewModel.res.observe(this) { response ->
//            _binding.appRecyclerView.apply {
//                layoutManager = LinearLayoutManager(context)
//                setHasFixedSize(true)
//                adapter = MainDemoListAdapter(context, response)
//            }
        }
}