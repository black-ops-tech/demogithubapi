package com.example.demogitrepodetail

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DemoGitRepoApplication : Application() {}