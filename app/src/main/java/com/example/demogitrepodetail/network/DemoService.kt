package com.example.demogitrepodetail.network

import com.example.demogitrepodetail.model.DataModel
import com.example.demogitrepodetail.util.DemoConstant.BASE_URL
import com.example.demogitrepodetail.util.DemoConstant.URL_SEARCH
import retrofit2.Call
import retrofit2.http.GET

interface DemoService {
    @GET(URL_SEARCH)
    fun getData(): Call<DataModel>
}